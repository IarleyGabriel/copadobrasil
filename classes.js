class Time{
    constructor(localização,nome){
        this.localização = localização;
        this.nome = nome;
    }
}

let time1 = new Time("caixaBozo","Piraquistel");
let time = new Time("caixaBozo","OutroTime");

class Estadio{
    constructor(nomeEstadio){
        this.nomeEstadio = nomeEstadio;
        
    }
}

let estadio = new Estadio("Mineirão");

class Jogo{
    constructor(jogadores,inicio,fim,juiz,times){
        this.jogadores = jogadores;
        this.inicio = inicio;
        this.fim = fim;
        this.juiz = juiz;
        this.times = times;
    }
}

class Jogador{
    constructor(numeroJogador,nomeJogador,numGols,n_amarelos,vermelhos){
        this.numeroJogador = numeroJogador; 
        this.numGols = numGols;
        this.nomeJogador = nomeJogador;
        this.vermelhos = vermelhos;
    }
}

let jogador1 = new Jogador(55,"Fulano",47,2,1);
let jogador2 = new Jogador(49,"Sicrano",58,1,0);

let jogo = new Jogo([jogador1,jogador2],"primeiroTempo","finalSegundoTempo","Arbitro",[time1,time2]);